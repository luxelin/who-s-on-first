		var code = '';
		$(document).ready(function() {
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		
		function objToString (obj) {
			console.log(Object.keys(obj).length);
			var str = '{';
			var i = 0;
			var sep = ',';
			for (var p in obj) {
				if (obj.hasOwnProperty(p)) {
					if(i == (Object.keys(obj).length) - 1) sep = '';
					if(typeof(obj[p]) === 'function') {
						str += p + ':'+obj[p] + sep;
					}
					else if(typeof(obj[p]) === 'string') {
						str += p + ':"' + obj[p] + '"' + sep;
					}
					else {
						str += p + ':' + obj[p] + sep
					}
					i++;
				}
			}
			str += '}';
			return str;
		}
		
		var i = 9;
		var option = {
			'corner' : false,
			'wide' : false,
			'line': false,
			'arrows': false,
			'deletebtn': false,
			'column': function(container) {
				if($(container).width() > 768) return 2;
				else return 1;
			},
			'first' : 'left',
			'animation' : false,
			'duration': 2000
		}
		
		$('input[name="corner"]').change(function() {
			option.corner = $('input[name="corner"]').is(':checked')?'.corner':false;
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('input[name="wide"]').change(function() {
			option.wide = $('input[name="wide"]').is(':checked')?'.wide':false;
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('input[name="line"]').change(function() {
			option.line = $('input[name="line"]').is(':checked');
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('input[name="arrows"]').change(function() {
			option.arrows = $('input[name="arrows"]').is(':checked');
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('input[name="deletebtn"]').change(function() {
			option.deletebtn = $('input[name="deletebtn"]').is(':checked')?'icon-remove':false;
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('input[name="first"]').change(function() {
			option.first = $('input[name="first"]:checked').val();
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('select[name="animation"]').change(function() {
			option.animation = ($('select[name="animation"] option:selected').val() != '0')?$('select[name="animation"] option:selected').val():false;
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('input[name="duration"]').change(function() {
			option.duration = parseInt($('input[name="duration"]').val());
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
		});
		$('[data-action="init"]').click(function() {
			code = '$(\'#timeline\').timeline('+objToString(option)+');';
			preview();
			var optMore = option;
			optMore.onRemoveItem = function() {
				code = "$('#timeline').timeline('removeItem', $(selector));";
				preview();
			}
			$('#timeline').timeline(optMore);
		});
		
		$('[data-action="prepend"]').click(function() {
			var nbr = Math.round(Math.random()*5);
			
			$.ajax({
				url:'http://sheenlipsum.com/gettext/'+nbr,
				type: 'GET',
				dataType: 'JSON'
			}).done(function(lorem) {
				if(lorem.length) {
					for(l in lorem) lorem[l] = '<p>'+lorem[l]+'</p>';
					lorem = lorem.join('');
				}
				$('#timeline').timeline('prepend', {element:'<div class="block"><div class="inner"><h2>'+(parseInt(i)+1)+'</h2>'+lorem+'</div></div>', callback: function(elm) {
						$('<p class="callback" style="color:red;">Hello I am a callback function for prepend !</p>').appendTo($(elm).children('.inner')).hide().fadeIn(3000, function() {
							$(this).remove();
						});
					}
				});
				i++;
				
			}).fail(function() {
				var lorem = 'Can\'t connect to lorem ipsum api, but we don\'t care, right ?';
				$('#timeline').timeline('prepend', {element:'<div class="block"><div class="inner"><h2>'+(parseInt(i)+1)+'</h2>'+lorem+'</div></div>', callback: function(elm) {
						$('<p class="callback" style="color:red;">Hello I am a callback function for prepend !</p>').appendTo($(elm).children('.inner')).hide().fadeIn(3000, function() {
							$(this).remove();
						});
					}
				});
				i++;
			});
			code = "$('#timeline').timeline('prepend', {element:'<div class=\"block\"><div class=\"inner\"><h2>Title</h2>Content</div></div>', callback: function(elm) {"+
					"$('<p class=\"callback\" style=\"color:red;\">Hello I am a callback function for prepend !</p>').appendTo($(elm).children('.inner')).hide().fadeIn(3000, function() {"+
						"$(this).remove();"+
					"});"+
				"}"+
			"});";
			preview();
		});

// bcf -- modified the code here to run at intervals
var twcb = function() {
    var nbr = Math.round(Math.random()*5); 
    $.ajax({
				url:'http://sheenlipsum.com/gettext/'+nbr,
				type: 'GET',
				dataType: 'JSON'
			}).done(function(lorem) {
				if(lorem.length) {
					for(l in lorem) lorem[l] = '<p>'+lorem[l]+'</p>';
					lorem = lorem.join('');
				}
				$('#timeline').timeline('append', '<div class="block"><div class="inner"><h2>'+(parseInt(i)+1)+'</h2>'+lorem+'</div></div>');
				i++;
				
			}).fail(function() {
				var lorem = 'Can\'t connect to lorem ipsum api, but we don\'t care, right ?';
				$('#timeline').timeline('append', '<div class="block"><div class="inner"><h2>'+(parseInt(i)+1)+'</h2>'+lorem+'</div></div>');
				i++;
			    });
}
			    

			    
		$('[data-action="append"]').click(function() {
			console.log("appending....");
						setInterval(twcb, 5000);
		    });

		
		$('[data-action="destroy"]').click(function() {
			$('#timeline').timeline('destroy');
			code = "$('#timeline').timeline('destroy');";
			preview();
		});
		
		$('.toggle-slide').on('click', function() {
			$('header section').slideToggle(); 
			$(this).toggleClass('icon-chevron-up').toggleClass('icon-chevron-down');
			if($(this).hasClass('icon-chevron-up')) $('.content').animate({marginTop: '+=320'});
			else $('.content').animate({marginTop: '-=320'});
		}).on('mouseenter mouseleave', function() {
			$(this).toggleClass('icon-white');
		});
		
		$('.shortcut').on('mouseenter mouseleave', function() {
			$(this).toggleClass('icon-white');
		});
		
		function preview() {
			code = js_beautify(code);
			$('.render pre code').text(code);
			$('pre code').each(function(i, e) {hljs.highlightBlock(e)});
		}