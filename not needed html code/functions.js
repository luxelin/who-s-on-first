
$("#myInput").keyup(function(event){
    if(event.keyCode == 13){
        parseQuestion();
    }
});

var holdMyQuestion;
var checkMyQuestion;

function parseQuestion(){
  document.getElementById("results").innerHTML = "<p></p>";
  checkQuestionWords();

  $("myQuestionButton").click();
  
  if (checkMyQuestion == true){
    document.getElementById("theAlert").innerHTML = "<h6></h6";
    destroyVerbs();
    destroyPronouns();   
    callTheQuestion();}
  else{
    document.getElementById("results").innerHTML = "<p></p>";
    errorInCallingQuestion();
      }
}
function checkQuestionWords() {
    var theQuestion = document.getElementById("myInput").value;
    theQuestion = theQuestion.trim();
    theQuestion = theQuestion.toLowerCase();
    var questionWords = ["who", "what", "where", "when", "why", "how", "at what", "by when", "for what"];
    var isItGood = false;

    for (var i=0; i<questionWords.length; i++){
      var holder1 = questionWords[i];
      var holder2 = holder1.length;
        if (theQuestion.substr(0,holder2) == questionWords[i]){
          isItGood = true;
          break;}/*
        if (theQuestion.substr(0,holder2) == holder1){
          isItGood = true;
          checkMyQuestion = true;
          break;
        }*/
        else{
          isItGood = false;
          continue;}}

    var lengthOfQuestion = theQuestion.length;
    if (theQuestion.substr((lengthOfQuestion-1),1)=="?"){
      theQuestion = theQuestion.substr(0,(lengthOfQuestion-1)) + " ";
    }
    else{
      theQuestion = theQuestion; 
    }


    if (isItGood == false){
      checkMyQuestion = false;
    }
    else{
      document.getElementById("results").innerHTML = "#" + theQuestion;
      checkMyQuestion = true;
    }
    holdMyQuestion = theQuestion;
    return;
}
function destroyVerbs(){
  var verbs = ["did", "does", "do", "has", "have", "had", "will", "would", "can", "may", "might", "is", "are", "come"];
  var totalVerbNumber = verbs.length;

  for (var i = 0; i <totalVerbNumber; i++){
    var ttt = " " + verbs[i] + "";
    if (holdMyQuestion.match(ttt) != -1){
      holdMyQuestion = holdMyQuestion.replace(ttt, "");
    }
    else{
      holdMyQuestion = holdMyQuestion;}
    }
  holdMyQuestion = holdMyQuestion.trim();
}
function destroyPronouns(){
  var pronouns = ["I", "my", "mine", "you", "yours", "your", "he", "his", "him", "she", "hers", "her", "it", "its", "it's", "we", "us", "our", "ours", "them", "their", "they", "the", "a"]
  var totalPronounNumber = pronouns.length;

  for (var i = 0; i <totalPronounNumber; i++){
    var ttt = " " + pronouns[i] + " ";
    if (holdMyQuestion.match(ttt) != -1){
      holdMyQuestion = holdMyQuestion.replace(ttt, " ");
    }
    else{
      holdMyQuestion = holdMyQuestion;}
    }
}
function callTheQuestion(){
  holdMyQuestion = holdMyQuestion.trim();
  var length = holdMyQuestion.length;

  for (var i = 0; i<length; i++){
    if (holdMyQuestion.substr(i,1) == " "){
      var clength = holdMyQuestion.length;
      holdMyQuestion = holdMyQuestion.substr(0,i) + " #" + holdMyQuestion.substr((i+1),(clength-i-1));
    }
    else{
      holdMyQuestion = holdMyQuestion;
    }
  }
  holdMyQuestion = "#" + holdMyQuestion;
  document.getElementById("results").innerHTML = holdMyQuestion;
}
function errorInCallingQuestion(){
  if (checkMyQuestion == false){
    bootstrap_alert = function() {}
    bootstrap_alert.danger = function(message) {
        $('#theAlert').html('<div class="alert alert-block alert-error fade in"><button class="close" data-dismiss="alert" id="clickToDismissAlert">&times;</button><span>'+message+'</span></div>');
    }
    $('#myQuestionButton').on('click', function() {
            bootstrap_alert.danger("<h4 class=\"alert-heading\">Oh snap! That didn't go over well!</h4><p>Please enter a question beginning with who, what, where, when, why, or how.</p>");
    });
  }
  else{
    $("theAlert").alert('close');

  }
}
function dismissTheAlert(){
  $("myQuestionButton").click();
  document.getElementById("clickToDismissAlert").click();
}